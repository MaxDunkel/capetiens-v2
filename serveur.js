var express = require('express');
var server = express();
var fs = require('fs');


server.use(express.urlencoded());
server.use(express.json());


server.use('/static',express.static('static'));

server.get('/index.html', function (request, response) {
  response.sendfile('index.html');
});

server.get('/capet.html', function (request, response) {
  response.sendfile('capet.html');
});

server.get('/auguste.html', function (request, response) {
  response.sendfile('auguste.html');
});

server.get('/lebel.html', function (request, response) {
  response.sendfile('lebel.html');
});

server.get('/louisix.html', function (request, response) {
  response.sendfile('louisix.html');
});

server.get('/formulaire.html', function (request, response) {
  response.sendfile('formulaire.html');
});

server.get('/mentionslegales.html', function (request, response) {
  response.sendfile('mentionslegales.html');
});

server.post('/formulaire.html', function (request, response) {
  const dataform = request.body;
  const data = JSON.stringify(dataform);
  fs.writeFileSync('form.json', data);
});


server.listen(8050);
